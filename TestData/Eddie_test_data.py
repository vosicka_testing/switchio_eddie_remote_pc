#!/usr/bin/python
# -*- coding: utf-8 -*-

class HomePageData:

    test_01_administrator = [
        {"login":"vosicka","password":"Sxapk181998@"}]

    test_02_servis = [
        {"login":"auttestyservis","password":"Sxapk181996@"}]

    test_03_servis_terminal = [
        {"login": "auttestyservisterm", "password": "Sxapk181996@"}]

    test_04_user = [
        {"login": "auttestyuser", "password": "Sxapk181996@"}]

    test_05_user_terminal = [
        {"login": "auttesty", "password": "Sxapk181995"}]

    test_06_sftp_automatic = [
        {"network": "TMONET", "type": "INFO21", "term_id": "MOTEST01", "passwd": "EDDIE", "port": 7820},
        {"network": "TSB", "type": "INFO21", "term_id": "SBEPNGT001", "passwd": "M0NET2SB", "port": 7320},
        #{"network": "TFISKALPRO", "type": "INFO20","term_id": "SBEPNGT001", "passwd": "M0NET2SB", "port": 7920},
        {"network": "TPLUSPRO", "type": "INFO21", "term_id": "PPROSA03", "passwd": "EDDIE", "port": 7220},
        {"network": "TMOS", "type": "INFO21", "term_id": "TMOS0014", "passwd": "EDDIE", "port": 7121},
        {"network": "TMOL", "type": "INFO21", "term_id": "SVNT0003", "passwd": "EDDIE", "port": 7122},
        {"network": "TSONDA", "type": "INFO21", "term_id": "TCRE1101", "passwd": "a5S3Cc02Oi9", "port": 7321},
        {"network": "TCROSSPARK", "type": "INFO21", "term_id": "TCRNL002", "passwd": "EDDIE", "port": 7322},
        {"network": "TASSECO", "type": "INFO21", "term_id": "TASTYPL001", "passwd": "a5S3Cc02Oi9", "port": 7324},
        {"network": "TAUTOGARD", "type": "INFO21", "term_id": "TAUT0002", "passwd": "EDDIE", "port": 7325},
        {"network": "TGREENCENTER", "type": "INFO21", "term_id": "TGC30001", "passwd": "EDDIE", "port": 7326},
        {"network": "TPREMIUMFIT", "type": "INFO21", "term_id": "TPF30001", "passwd": "EDDIE", "port": 7124}
    ]
    test_07_sftp_manual = [
        {"network": "TMONET", "type": "INFO21", "term_id": "MOTEST01", "passwd": "EDDIE", "port": 7820},
        {"network": "TSB", "type": "INFO21", "term_id": "SBEPNGT001", "passwd": "M0NET2SB", "port": 7320},
        {"network": "TFISKALPRO", "type": "INFO20","term_id": "A3SK0001", "passwd": "heslo123", "port": 7920},
        {"network": "TPLUSPRO", "type": "INFO21", "term_id": "PPROSA03", "passwd": "EDDIE", "port": 7220},
        {"network": "TUPC", "type": "INFO20", "term_id": "RBIXATTT01", "passwd": "SONUPXSO", "port": 7422},
        {"network": "TMPAS", "type": "INFO20", "term_id": "BGCERTTID1", "passwd": "12345", "port": 7620},
        {"network": "PRN-TEST", "type": "INFO20", "term_id": "ESLTPRSK05", "passwd": "test123", "port": 7520},
        {"network": "TSMPOS", "type": "INFO21", "term_id": "SMP00078", "passwd": "6UxVjf8KNI", "port": 7120},
        {"network": "TMOS", "type": "INFO21", "term_id": "TMOS0014", "passwd": "EDDIE", "port": 7121},
        {"network": "TMOL", "type": "INFO21", "term_id": "SVNT0003", "passwd": "EDDIE", "port": 7122},
        {"network": "TSONDA", "type": "INFO21", "term_id": "TCRE1101", "passwd": "a5S3Cc02Oi9", "port": 7321},
        {"network": "TCROSSPARK", "type": "INFO21", "term_id": "TCRNL002", "passwd": "EDDIE", "port": 7322},
        {"network": "TSWITCHIODEMO", "type": "INFO21", "term_id": "TPRPN001", "passwd": "dPrN3XG01", "port": 7821},
        {"network": "TINSDEV", "type": "INFO21", "term_id": "ANDTOK02", "passwd": "EDDIE", "port": 7323},
        {"network": "TASSECO", "type": "INFO21", "term_id": "TASTYPL001", "passwd": "a5S3Cc02Oi9", "port": 7324},
        {"network": "TUBA", "type": "INFO21", "term_id": "TUBAM002", "passwd": "EDDIE", "port": 7123},
        {"network": "TSOLITEAPAY", "type": "INFO20", "term_id": "SPTEST02", "passwd": "trustno1", "port": 7921},
        {"network": "TPROKASA", "type": "INFO20", "term_id": "BP2E9926", "passwd": "trustno1", "port": 7922},
        {"network": "TAUTOGARD", "type": "INFO21", "term_id": "TAUT0002", "passwd": "EDDIE", "port": 7325},
        {"network": "TGREENCENTER", "type": "INFO21", "term_id": "TGC30001", "passwd": "EDDIE", "port": 7326},
        {"network": "TFISKALPRO_SIACZ", "type": "INFO20", "term_id": "A3TCA001", "passwd": "heslo123", "port": 7923},
        {"network": "TFISKALPRO_SIASK", "type": "INFO21", "term_id": "A3NAA003", "passwd": "heslo123", "port": 7924},
        {"network": "TPREMIUMFIT", "type": "INFO21", "term_id": "TPF30001", "passwd": "EDDIE", "port": 7124},
        {"network": "TBCCR", "type": "INFO21", "term_id": "TBCCR001", "passwd": "BCCR123", "port": 7327}
    ]
#!/usr/bin/python
# -*- coding: utf-8 -*-

from utilities.BaseClass import BaseClass
from pageObjects.eddie_sftp_automatic_06 import EddieSFTPAutomatic06
from TestData.Eddie_test_data import HomePageData
import time
import pytest


class Test06(BaseClass):
    def test_sftp_automatic(self, getData):
        tc06 = EddieSFTPAutomatic06(self.driver, getData["network"], getData["type"], getData["term_id"], getData["passwd"], getData["port"])
        # logger
        log = self.getLogger()
        value = tc06.sftp_main_function()

        try:
            assert value.replace(".", "").strip().lower() == "tms automatic call was success"
            log.info("TMS AUTOMATIC call was success")
            state = True

        except:
            log.warning("Spatna odpoved v PRNSRV - Network: " + getData["network"] + "ID: " + getData["term_id"])
            log.warning(value)
            state = False
        assert state

    @pytest.fixture(params=HomePageData.test_06_sftp_automatic)
    # @pytest.fixture(params=HomePageData.getTestData('TestCase1'))
    def getData(self, request):
        return request.param